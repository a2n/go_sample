package main

import (
    "os"
)

func f1(v string) {
    f, err := os.OpenFile("/tmp/a.txt", os.SEEK_SET, os.ModeAppend)
    cherr(err)

    _, err = f.Write([]byte(v))
    cherr(err)
    err = f.Close()
    cherr(err)
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}

func f2() {
    path := "/tmp/test.txt"
    file, err := os.Open(path)
    if !os.IsExist(err) {
	file, err = os.Create(path)
    }
    defer file.Close()
}

func main() {
    f1("a")
}
