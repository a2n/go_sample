package main

import (
    "path/filepath"
    "fmt"
    "path"
    "os"
)

func foo() {
    path := "/usr/local/foo.txt"
    dir, file := filepath.Split(path)
    fmt.Printf("dir: %s\nfile: %s\n", dir, file)
}

func f1() {
    p1 := "/usr/local/"
    p2 := "foo.txt"
    p3 := "f1.txt"
    fmt.Println(path.Join(p1, p2, p3))
}

func f2() {
    str, err := filepath.Abs(os.Args[0])
    if err != nil {
	panic(err)
	return
    }

    fmt.Printf("path: %s\n", str)
}

func f3() {
    f, e := os.Open("/tmp")
    defer f.Close()
    if e != nil {
	panic(e)
	return
    }

    stat, e := f.Stat()
    if e != nil {
	panic(e)
	return
    }
    fmt.Printf("name: %s\n", stat.Name())
}

func f4() {
    s, e := filepath.Abs(".")
    if e != nil {
	panic(e)
	return
    }
    fmt.Printf("path: %s\n", s)
}

func main() {
    f1()
}
