package main

import (
    "fmt"
    //"encoding/json"
    "math/rand"
    "time"
)

type Product struct {
    Name	string
    Parent	string
}

type Tag struct {
    Name	string
    Parent	string
}

func makeProduct(parent string) *Product{
    rand.Seed(time.Now().UnixNano())
    return &Product {
	fmt.Sprintf("Product%d", rand.Int31()),
	parent,
    }
}

func makeTag(parent string) *Tag {
    rand.Seed(time.Now().UnixNano())
    return &Tag {
	fmt.Sprintf("Tag%d", randInt31()),
	parent,
    }
}

func main() {
    t1 := makeTag("")
    t2 := makeTag("t1")
    p1 := makeProduct("t1")
    p2 := makeProduct("t2")
}
