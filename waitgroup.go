package main

import (
	"sync"
	"fmt"
)

func main() {
	var wg sync.WaitGroup
	var numbers = []uint8{0, 1, 2, 3, 4, 5, 6, 7, 8}

	for _, n := range numbers {
		wg.Add(1)
		go func(n uint8) {
			fmt.Println(n)
			wg.Done()
		}(n)
	}

	wg.Wait()
}
