package main

import (
    "encoding/json"
    "fmt"
    "time"
)

func encode(cs []Cookie) []byte {
    b, err := json.Marshal(cs)
    if err != nil {
	panic(err)
    }
    return b
}

func decode(cs []byte) []Cookie {
    var cookies []Cookie
    err := json.Unmarshal(cs, &cookies)
    if err != nil {
	panic(err)
    }
    return cookies
}

func gen() []Cookie {
    c1 := Cookie {
	"Cookie 1",
	time.Now(),
    }

    c2 := Cookie {
	"Cookie 2",
	time.Now(),
    }
    return []Cookie{c1, c2}
}

type Cookie struct {
    Name	    string
    Expires	    time.Time
}

func main() {
    bs := encode(gen())
    str := decode(bs)
    fmt.Printf("%s\n", str)
}
