package main

import (
	"web-robots"
	"fmt"
)

func main() {
	r := robots.NewRobots()

	url := "https://www.google.com/search/a"
	if !r.IsAllowURLString("*", url) {
		fmt.Println(url + " is not a allow url.")
	}

	url = "https://www.google.com/search/about/"
	if !r.IsAllowURLString("*", url) {
		fmt.Println(url + " is a allow url.")
	}

	url = "https://www.yahoo.com/p/a"
	if r.IsAllowURLString("*", url) {
		fmt.Println(url + " is not a allow url.")
	}

	url = "https://www.yahoo.com/a"
	if !r.IsAllowURLString("*", url) {
		fmt.Println(url + " is a allow url.")
	}
}
