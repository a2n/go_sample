package main

import (
    "fmt"
    "time"
)

func foo() {
    fmt.Printf("%d\n", time.Now().Unix())
}

func f2() {
    n := time.Now()
    today := fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", n.Year(), n.Month(), n.Day(), n.Hour(), n.Minute(), n.Second())
    fmt.Printf("today: %s\n", today)
}

func f3() {
    t := time.Unix(1, 2)
    fmt.Printf("%d\n", t.Second())
    fmt.Printf("%d\n", t.Nanosecond())
}

func main() {
    f3()
}
