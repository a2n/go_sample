package main

import (
    "fmt"
    "crypto/md5"
    "io"
)

type Person struct {
    Id		string
    Name	string
}

func f3() {
    ps := []Person {
	{"dean", "小龐"},
	{"amy", "Amy"},
	{"ken", "Ken"},
	{"aha", "阿霞"},
	{"daniel", "證霖"},
	{"jerry", "子慶"},
	{"tom", "勝堂"},
	{"bill", "小閔"},
	{"a0982665517", "伯弦"},
	{"stella", "湘湘"},
	{"beryl", "小洪"},
	{"lim", "小玲"},
	{"lisa", "品蓉"},
	{"boss", "老大"},
	{"june", "俊昂"},
	/*
	{"dean", "環球網路 何姿樺"},
	{"amy", "環球網路 顏鈺臻"},
	{"ken", "環球網路 李柏穎"},
	{"aha", "環球網路 李佩霞"},
	{"daniel", "環球網路 黃證霖"},
	{"jerry", "環球網路 楊子慶"},
	{"tom", "環球網路 洪勝堂"},
	{"bill", "環球網路 詹祐閔"},
	{"a0982665517", "環球網路 賴伯弦"},
	{"stella", "環球網路 林湘樺"},
	{"beryl", "環球網路 洪敏芳"},
	{"lim", "環球網路 王小玲"},
	{"lisa", "環球網路 余品蓉"},
	{"boss", "環球網路 林晨瀚"},
	{"june", "環球網路 賴俊昂"},
	*/
    }
    for _, v := range ps {
	h := md5.New()
	io.WriteString(h, v.Id)
	fmt.Printf("('%s@worldnet.tw', '%x', '%s', 'tw.worldnet.%s', '%s'),\n", 
	    v.Id, h.Sum(nil), v.Name, v.Id, v.Id)
	/*
	fmt.Printf("CREATE DATABASE %s;\n", v.Id)
	fmt.Printf("mysql -uroot -pEc2+-x/2012 %s < test.sql\n", v.Id)
	fmt.Printf("use %s;\n", v.Id)
	fmt.Printf("INSERT INTO Function VALUES (0, '關於%s', '/about/edit'), (1, '聯絡%s', '/contact/edit/'), (2, '%s的消息', '/news/edit/'), (3, '%s的產品', '/product/edit/'), (4, '圖片管理', '/file/edit/'), (5, '首頁輪動', '/ad/edit/'), (99, '登出', '/auth/logout/');\n",
	    v.Name, v.Name, v.Name, v.Name)
	*/
    }
}

func f2() {
    for i := 1; i < 4; i++ {
	h := md5.New()
	text := fmt.Sprintf("ninanails%d", i)
	io.WriteString(h, text)
	fmt.Printf("%s: %x\n", text, h.Sum(nil))
    }
    fmt.Println()
}

func f1() {
    h := md5.New()
    io.WriteString(h, "annkitty151688")
    fmt.Printf("%x\n", h.Sum(nil))
}

func f4() {
    pwds := []string {
	"c6WH14o4I0xA5g7",
	"J616n1q5nQ4s57x",
	"xx582P4I3uftT3l",
	"4cY0KjjWuPk53km",
	"X3s5DO3v25021g8",
    }

    for _, v := range pwds {
	h := md5.New()
	io.WriteString(h, v)
	fmt.Printf("%s: %x\n", v, h.Sum(nil))
    }
}

func main() {
    f1()
}
