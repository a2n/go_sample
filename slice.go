package main

import (
    "fmt"
    "time"
    "log"
)

func del1() {
	s := []int{1, 3}
	s = append(s[:0], s[1:]...)
	fmt.Println(s)
	s = append(s[:0], s[1:]...)
	fmt.Println(s)
}

func twoArray(s []int) []int{
    /* 
	Slice is a descriptor that points to the array that stores the data.
	Two arrays menas two slices.
    */
    s1 := make([]int, 0)
    for _, v := range s {
	if v % 2 > 0 {
	    s1 = append(s1, v)
	}
    }
    return s1
}

func oneArray(s []int) []int{
    for k, v := range s {
	if v % 2 == 0 {
	    s = append(s[:k], s[k+1:]...)
	}
    }
    return s
}

type File struct {
    Name	string
}

func del() {
    s := []int{ 1, 3, 5, 7 }
    s = append(s[:0], s[2:]...)
    for k, v := range s {
	fmt.Printf("%d: %d\n", k, v)
    }
}

func addNDeleteWithTimer() {
    thatTime := time.Now()
    for {
	c := time.Tick(1 * time.Second)
	for now := range c {
	    log.Printf("seconds: %f\n", now.Sub(thatTime).Seconds())
	}
    }
}

func f0() {
	f00("a", "b", "c")
}

func f00(slice ...string) {
	for _, s := range slice {
		fmt.Println(s)
	}
}

func main() {
	del1()
}
