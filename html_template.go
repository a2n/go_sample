package main

import (
    "html/template"
    "os"
)

const html = `{{range .}}My name is {{.Name}}, I'm {{.Age}} years old.
{{end}}`

type Person struct {
    Name    string
    Age	    int
}

func main() {
    t := template.New("new")
    t, err := t.Parse(html)
    checkErr(err)

    ps := []Person {
	{"Alan", 28},
	{"Yvonne", 29},
    }

    err = t.Execute(os.Stdout, ps)
    checkErr(err)
}

func checkErr(err error) {
    if err != nil {
	panic(err)
    }
}
