package main

import (
	"log"
	"net/http"
	"net/url"
	"sync"

	//"github.com/gorilla/websocket"
	"golang.org/x/net/websocket"
)

var l *sync.Mutex
func main() {
	log.SetFlags(log.Lmicroseconds | log.Lshortfile)
	l = new(sync.Mutex)

	open()
	defer close()

	go func() {
		for i := 0; i < 1; i++ {
			send()
		}
	}()

	for {
		read()
	}
}

var conn *websocket.Conn
func open() {
	header := http.Header {
		"Cookie": []string{"JSESSIONID=434C424EEF5EC5DCEDED1D80F278893A",},
	}

	location, err := url.Parse("ws://shwoo.gov.taipei:80/shwoo/websocket")
	if err != nil {
		panic(err)
	}

	origin, err := url.Parse("http://shwoo.gov.taipei/shwoo/product/product00/bidlist")
	if err != nil {
		panic(err)
	}

	config := websocket.Config {
		Location: location,
		Origin: origin,
		Header: header,
		Version: websocket.ProtocolVersionHybi13,
	}

	log.Println("Dialing...")
	conn, err = websocket.DialConfig(&config)
	if err != nil {
		panic(err)
	}
}

func close() {
	if err := conn.Close(); err != nil {
		panic(err)
	}

	log.Println("Closed.")
}

func send() {
	n, err := conn.Write([]byte(`{"no":1,"auid":188851}`))
	if err != nil {
		panic(err)
	}
	log.Printf("write %d bytes\n", n)

}

func read() {
	l.Lock()

	msg := make([]byte, 1024)
	n, err := conn.Read(msg)
	if err != nil {
		panic(err)
	}

	log.Printf("%s\n", msg[:n])

	l.Unlock()
}
