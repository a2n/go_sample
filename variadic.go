package main

import (
	"fmt"
)

func main() {
	f0()
}

func f0() {
	f1("Alan", "a", "b", "c")
}

func f1(name string, vals ...string) {
	for k, v := range vals {
		fmt.Printf("%d: %s\n", k, v)
	}

	fmt.Printf("Name: %s\n", name)
}
