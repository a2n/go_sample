package main

import (
	"fmt"
	"io/ioutil"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	blogger "github.com/google/google-api-go-client/blogger/v3"
)

func main() {
	const clientId = "537358248201-1rdhn7o4t19tgl1p9ve2ur294o7q9jkr.apps.googleusercontent.com"
	b, err := ioutil.ReadFile("client_secret_537358248201-1rdhn7o4t19tgl1p9ve2ur294o7q9jkr.apps.googleusercontent.com.json")
	if err != nil {
		panic(err)
	}

	config, err  := google.ConfigFromJSON(b, blogger.BloggerScope)
	if err != nil {
		panic(err)
	}

	fmt.Println(config.AuthCodeURL("GET", oauth2.AccessTypeOnline))

	var code string
	fmt.Print("Paste the code here: ")
	_, err = fmt.Scanf("%s", &code)
	if err != nil {
		panic(err)
	}

	token, err := config.Exchange(oauth2.NoContext, code)
	if err != nil {
		panic(err)
	}
	fmt.Printf("token: %s\n", token)

	client := config.Client(oauth2.NoContext, token)
	s, err := blogger.New(client)
	if err != nil {
		panic(err)
	}

	ps := blogger.NewPostsService(s)
	post := &blogger.Post {
		Title: "abc",
		Content: "def",
	}
	// https://groups.google.com/forum/#!topic/bloggerdev/nXAwHPbZ43A
	pic := ps.Insert("6863276", post)
	pic.Fields("title", "content")
	_, err = pic.Do()
	if err != nil {
		panic(err)
	}
}
