package main

import (
    "fmt"
    "time"
)

func main() {
    //foo()
    f2()
}

func f2() {
    v := make(chan bool)

    for {
	go check(v)
	select {
	case b := <-v:
	    if b == true {
		fmt.Println(b)
	    }
	}
    }
}

func check(v chan bool) {
    v <- time.Now().Second() == 0
}

func f1(v chan bool) {
    v <- time.Now().UnixNano() % 2 == 0
}

func foo() {
    b1 := make(chan bool)

    go f1(b1)
    go f1(b1)
    go f1(b1)
    go f1(b1)

    fmt.Printf("%t\n", <-b1)
    fmt.Printf("%t\n", <-b1)
    fmt.Printf("%t\n", <-b1)
    fmt.Printf("%t\n", <-b1)
}
