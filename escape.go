package main

import (
    "fmt"
    "strings"
)

func foo() {
    str := "1 2.png"
    str1 := strings.Replace(str, " ", "\\ ", -1)
    fmt.Printf("%s\n", str1)
}

func main() {
    foo()
}
