package main

import (
    "fmt"
    "io/ioutil"
    "io"
    "path"

    /* AWS Packages */
    "launchpad.net/goamz/aws"
    "launchpad.net/goamz/s3"
)

func main() {
    foo()
    //delAll()
}

func foo() {
    auth := aws.Auth{
	"AKIAIC35LKPCKBKPQ4LQ",
	"0cdMbWJkmEk0Yf7WnoYe5QTd1RTdn3Kj1vuN6NUX",
    }
    b := s3.New(auth, aws.APNortheast).Bucket("worldnet.tw")
    ls(".", b)
}

func ls(path_string string, b *s3.Bucket) {
    fi, err := ioutil.ReadDir(path_string)
    if err != nil && err != io.EOF {
	cherr(err)
    }
    for _, v := range fi {
	p := path.Join(path_string, v.Name())
	if v.IsDir() == true {
	    ls(p, b)
	} else {
	    send(p, b)
	    fmt.Println(p)
	}
    }
}

func send(fn string, b *s3.Bucket) {
    data, err := ioutil.ReadFile(fn)
    cherr(err)

    path := fmt.Sprintf("/test/%s", fn)
    err = b.Put(path, data, "", s3.Private)
    cherr(err)
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}
