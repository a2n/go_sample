package main

import (
	"fmt"
	"unicode/utf8"
	"strings"
	"regexp"
)

var address = "大安高工課桌椅中心1樓(臺北市大安區復興南路2段52號)"

func main() {
	f1()
}

func f1() {
	re := regexp.MustCompile(`[\p{Han}]{2,}[縣|市].+號`)
	fmt.Println(re.FindString(address))
}

func f0() {
	chars := make([]string, 0)

	left := 0
	right := 0
	for i := 0; ; i++{
		r, size := utf8.DecodeRuneInString(address)
		if r == utf8.RuneError {
			break
		}
		address = address[size:]

		char := fmt.Sprintf("%c", r)
		chars = append(chars, char)
		if char == "市" || char == "縣" {
			left = i - 2
		}
		if char == "號" {
			right = i + 1
		}
	}
	fmt.Printf("chars: %s\nlendth: %d\n%s", chars, len(chars), strings.Join(chars[left:right], ""))
}
