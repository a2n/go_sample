package main

import (
    "strings"
    "time"
    "fmt"
)

func main() {
    foo()
}

func foo() {
    v := []string{ "a", "b", "c" }
    values := make([]string, 0)
    date := fmt.Sprintf("%d", time.Now().Unix())

    for _, v := range v {
	values = append(values, fmt.Sprintf("('%s', '%s')", v, date))
    }
    stat := fmt.Sprintf("INSERT INTO MessagingReceived VALUES %s", strings.Join(values, ", "))
    stat += ";"
    fmt.Println(stat)
}
