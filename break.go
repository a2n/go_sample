package main

import (
    "fmt"
)

func foo() {
    v := true
    for v {
	if v {
	    v = false
	    fmt.Printf("v: %t\n", v)
	    break
	}
	fmt.Printf("after break.\n")
    }
}

func main() {
    foo()
}
