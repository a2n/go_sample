package main

import (
    "fmt"
    "errors"
    "net/http"
)

func foo(v int) {
    if v == 3 {
	panic(errors.New("3"))
    } else {
	fmt.Println(v)
    }
    fmt.Println(">>")
}

func foo2(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    v := r.FormValue("v")

    if v == "3" {
	panic(errors.New("3"))
    } else {
	w.Write([]byte(v))
    }
}

func main() {
    http.HandleFunc("/", foo2)
    http.ListenAndServe(":1024", nil)
}
