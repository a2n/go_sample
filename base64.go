package main

import (
    "log"
    "encoding/base64"
)

func main() {
    str := "Hello"
    tmp := base64.StdEncoding.EncodeToString([]byte(str))
    log.Printf("result: %s\n", tmp)
}
