package main

import (
	"fmt"
)

func main() {
	f0()
}

func f0() {
	defer fmt.Println(0); fmt.Println(1)
}
