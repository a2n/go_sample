package main

import (
	"net/http"
	"log"
	"time"

	"github.com/a2n/crawler"
	"github.com/a2n/alu"
)

func main() {
	//go server()
	f()
}

func  f() {
	l := alu.NewLogger("")
	config := &crawler.Config {
		Email: "a@b.com",
	}

	c := crawler.NewCrawler(config)

	//url := "http://127.0.0.1:8080/"
	url := "https://www.google.com"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		l.Printf("%s has error, %s.", alu.Caller(), err.Error())
		return
	}

	ticker := time.NewTicker(250 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			c.Push(req)
		case <-c.Response:
		}
	}
	/*
	const MAX = 20
	for i := 0; i < MAX; i++ {
		c.Push(req)
	}

	for i := 0; i < MAX; i++ {
		select {
		case <-c.Response:
		}
	}
	close(c.Response)
	*/
}

func server() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(500 * time.Millisecond)
		w.WriteHeader(http.StatusOK)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
