package main

import (
    "net/http"
    "fmt"
    "time"
    "io"
    "crypto/sha512"
)

func foo() http.Cookie {
    h := sha512.New()
    io.WriteString(h, fmt.Sprintf("%d", time.Now().Unix()))
    v := fmt.Sprintf("%x\n", h.Sum(nil))
    fmt.Println(v)
    d := int(time.Now().AddDate(0, 0, 14).Sub(time.Now()).Seconds())
    return http.Cookie {
	"token",				    /* Name */
	v,					    /* Value */
	"",					    /* Path */
	"",					    /* Domain */
	time.Now().AddDate(0, 0, 14),		    /* Expires */
	"",					    /* Raw Expires */
	d,					    /* Max Age */
	false,					    /* Secure */
	true,					    /* Http only */
	"",					    /* Raw */
	[]string{""},				    /* Unparsed */
    }
}

func Print(w http.ResponseWriter, r *http.Request) {
    c, err := r.Cookie("v")
    if err != nil {
	panic(err)
    }
    msg := fmt.Sprintf("cookie: %s\n", c.Value)
    w.Write([]byte(msg))
    /*
	Test method: curl -b v=123 localhost:1024/
    */
}

func giveCookie(w http.ResponseWriter, r *http.Request) {
    expire := time.Now().AddDate(0, 0, 1)
    cookie := http.Cookie{
	"token",
	"token1",
	"",
	"",
	expire,
	expire.Format(time.UnixDate),
	86400,
	false,
	true,
	"",
	[]string{""},
    }

    http.SetCookie(w, &cookie)
    w.WriteHeader(http.StatusOK)
}

func main() {
    http.HandleFunc("/", giveCookie)
    http.ListenAndServe(":1024", nil)
}
