package main

import (
    "net/http"
    "fmt"
    "io"
    "io/ioutil"
    "os"
    //"bufio"
    "bytes"
)

func check(err error) {
    if err != nil {
	panic(err)
    }
}

func h2(w http.ResponseWriter, r *http.Request) {
    err := r.ParseMultipartForm(10 << 20)
    check(err)

    file, fh, err := r.FormFile("f0")
    if err != nil {
	return
    }
    b, err := ioutil.ReadAll(file)
    check(err)
    //rd := bufio.NewReader(file)

    fmt.Printf("name: %s(%d)\n", fh.Filename, len(b))
    w.WriteHeader(http.StatusOK)
}

func h1(w http.ResponseWriter, r *http.Request) {
    reader, err := r.MultipartReader()
    if err != nil {
	fmt.Errorf(err.Error())
	return
    }

    for {
	part, err := reader.NextPart()
	if err == io.EOF {
	    fmt.Errorf("%s\n", err.Error())
	    return
	}

	fmt.Printf("%s\n", part.Header.Get("Content-Type"))

	buf := bytes.NewBufferString("")
	if buf == nil {
	    return
	}

	n, err := buf.ReadFrom(part)
	check(err)
	fmt.Printf("%s (%d)\n", part.FileName(), n)

	err = ioutil.WriteFile(part.FileName(), buf.Bytes(), os.ModePerm)
	if err != nil {
	    fmt.Errorf("Writing file (%s) encounted the error: %s\n", part.FileName(), err.Error())
	}
    }

    w.WriteHeader(http.StatusOK)
}

func main() {
    http.HandleFunc("/", h1)
    http.ListenAndServe(":1234", nil)
}
