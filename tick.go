package main

import (
	"fmt"
	"time"
)

func main() {
	f2()
}

func f2() {
	for {
		<-time.Tick(1 * time.Second)
		fmt.Println("tick")
	}
}

func f1() {
	select {
		case <-time.Tick(1 * time.Second):
		fmt.Println("tick")
	}
}

func f0() {
	t0 := time.NewTicker(1 * time.Second)
	n := 0
	go func() {
		for {
			<-t0.C
			fmt.Print(n)
			n++
		}
	}()

	t1 := time.NewTicker(5 * time.Second)
	select {
		case <-t1.C:
			fmt.Println("\n5 seconds, stop.")
			t0.Stop()
			t1.Stop()
	}
}
