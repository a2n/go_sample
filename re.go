package main

import (
    "regexp"
    "log"
)

func main() {
    f1()
}

func f1() {
    re, err := regexp.Match("[a-zA-Z0-9]{32}", []byte("60b725f10c9c85c70d97880dfe8191,3"))
    if err != nil {
	panic(err)
    }
    log.Printf("matched: %t\n", re)
}

func foo() {
    matched, err := regexp.MatchString("([aA]ndroid|i[pP]hone)", "Android")
    if err != nil {
	panic(err)
    }
    log.Printf("matched: %t\n", matched)
}
