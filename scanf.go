package main

import (
    "fmt"
)

func main() {
    str := "2013-05-16.json"
    var year, month, day int
    fmt.Sscanf(str, "%d-%d-%d.json", &year, &month, &day)
    fmt.Printf("%d\n%d\n%d\n", year, month, day)
}
