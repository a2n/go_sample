package main

import (
	"fmt"
)

func main() {
	f1()
}

func f1() {
	const MAX = 10
	ch := make(chan int, MAX)

	for i := 0; i < MAX; i++ {
		go func(v int) {
			ch<-v
		}(i)
	}

	for {
		fmt.Println(<-ch)
	}
}
