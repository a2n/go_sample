package main

import (
    "crypto/tls"
)

func main() {
    foo()
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}

func foo() {
    p1 := "data/3/cert.pem"
    p2 := "data/2/key.pem"

    _, err := tls.LoadX509KeyPair(p1, p2)
    cherr(err)
}
