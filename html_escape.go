package main

import (
    "html/template"
    "os"
    "fmt"
)

func main() {
    t := template.Must(template.New("ex").Parse(tmpl))
    v := map[string] interface{} {
	"Title":    "Test <b>World</b>",
	"Body":	    template.HTML("Hello <b>World</b>"),
    }
    t.Execute(os.Stdout, v)

    var str = "Hello <b>Alan</b>"
    var es = template.HTML(str)
    fmt.Printf("%s\n", es)
}

const tmpl = `<html>
<head>
<title>{{.Title}}</title>
</head>
<body>
    {{.Body}}
</body>
</html>`
