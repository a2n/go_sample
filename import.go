package main

import (
    "fmt"
    "io"
    A "time"
    M "crypto/md5"
)

func main() {
    v := 2
    if (v == 1) {
	fmt.Println(A.Now().Unix())
    } else {
	h := M.New()
	io.WriteString(h, "abc")
	fmt.Println(h.Sum(nil))
    }
}
