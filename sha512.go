package main

import (
    "crypto/sha512"
    "fmt"
    "io"
)

func main() {
    h := sha512.New()
    io.WriteString(h, "Hello")
    fmt.Printf("%x\n", h.Sum(nil))
}
