package main

import (
    "time"
    "fmt"
)

func foo1() {
    now := time.Now()
    t1 := now.AddDate(0, 0, 14)

    s := now.Format(time.ANSIC)
    s1 := t1.Format(time.ANSIC)

    tt, err := time.Parse(time.ANSIC, s)
    if err != nil {
	panic(err)
    }

    tt1, err := time.Parse(time.ANSIC, s1)
    if err != nil {
	panic(err)
    }

    if tt1.After(tt) {
	fmt.Printf("Yes.\n")
    } else {
	fmt.Printf("No.\n")
    }
}

func main() {
    foo1()
}
