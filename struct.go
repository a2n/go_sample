package main

import (
    "log"
)

func main() {
    foo()
}

type SS struct {
    Name string
    Age int
}

func foo() {
    array := make([]SS, 0)
    s := SS{"", 1}
    array = append(array, s)
    log.Printf("Name: '%s', Age: %d\n", array[0].Name, array[0].Age)
}
