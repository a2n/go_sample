package main

import (
    "log/syslog"
    "log"
    "os"
)

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}

func debug(text string) {
    log1(text, syslog.LOG_DEBUG)
}

func info(text string) {
    log1(text, syslog.LOG_INFO)
}

const PREFIX = "worldnet service "
func log1(text string, priority syslog.Priority) {
    w, err := syslog.New(priority, PREFIX)
    cherr(err)
    switch priority {
	case syslog.LOG_DEBUG:
	w.Debug(text)
	case syslog.LOG_INFO:
	w.Info(text)
    }
}

func foo() {
    debug("debug")
    info("info")
}

func f1() {
    path := "/tmp/test.txt"
    file, err := os.Open(path)
    defer func() {
	file.Close()
    }()
    if os.IsNotExist(err) {
	log.Printf("not exist.")
	file, err = os.Create(path)
    }

    /*
    ret, err := file.WriteString("string")
    if err != nil {
	panic(err)
    }
    log.Printf("writen %d bytes.\n", ret)
    */

    lgr := log.New(file, "f1", log.LstdFlags)
    lgr.Println("Hello a2n")
}

func main() {
    f1()
}
