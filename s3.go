package main

import (
    "fmt"
    "io/ioutil"

    /* AWS Packages */
    "launchpad.net/goamz/aws"
    "launchpad.net/goamz/s3"
)

func list(b *s3.Bucket) {
    list, err := b.List("", "", "", 1000)
    check(err)
    for _, v := range list.Contents {
	fmt.Printf("key: %s\n", v.Key)
    }
}

func check(err error) {
    if err != nil {
	panic(err)
    }
}

func put(b *s3.Bucket) {
    data, err := ioutil.ReadFile("upload.go")
    check(err)
    err = b.Put("/test.go", data, "", s3.Private)
}

func del(b *s3.Bucket) {
    err := b.Del("/worldnet.tw/test.go")
    check(err)
}

func main() {
    auth := aws.Auth{
	"AKIAIC35LKPCKBKPQ4LQ",
	"0cdMbWJkmEk0Yf7WnoYe5QTd1RTdn3Kj1vuN6NUX",
    }
    b := s3.New(auth, aws.APNortheast).Bucket("worldnet.tw")
    put(b)
    del(b)
    list(b)
}
