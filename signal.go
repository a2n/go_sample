package main

import (
	"fmt"
	"os"
	"os/signal"
)

func main() {
	ch := make(chan os.Signal)
	signal.Notify(ch, os.Kill, os.Interrupt)
	i := 0

	go func() {
		for {
			i++
		}
	}()

	s := <-ch
	fmt.Printf("\n%s\ni: %d\n\n", s.String(), i)
}
