package main

import (
    "fmt"
    "path/filepath"
)

func main() {
    foo()
}

func foo() {
    str := "aa/bb/cc.txt"
    fn := filepath.Base(str)
    ext := filepath.Ext(str)
    fn2 := fn[:len(fn) - len(ext)]
    fmt.Println(fn2)
}
