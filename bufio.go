package main

import (
    "bufio"
    "os"
)

func main() {
    foo()
}

func foo() {
    path := "/tmp/abc.txt"
    file, err := os.Open(path)
    if os.IsNotExist(err) {
	file, err = os.Create(path)
    }

    w := bufio.NewWriter(file)
    _, err = w.WriteString("abcd")
    if err != nil {
	panic(err)
    }

    err = w.Flush()
    if err != nil {
	panic(err)
    }
    file.Close()
}
