package main

import (
    "encoding/json"
    "fmt"
)

type Store struct {
    Name	string
    Address	string
    Telno	string
    Opening	string
}

func f0() {
    s1 := Store{"name1", "address1", "telno1", "opening1"}
    s2 := Store{"name2", "address2", "telno2", "opening2"}
    a := []Store{s1, s2}

    b, err := json.MarshalIndent(a, "", "\t")
    if err != nil {
	fmt.Printf("error: %s\n", err)
    }

    fmt.Printf("struct:\t%s\njson:\t%s\n", a, string(b))
}

func f1() {
	type A struct {
		Foo struct {
			Name string
		}
		Value int
	}

	str := []byte(`{"Value":0,"Foo":{"Name":"a2n"}}`)
	var a A
	json.Unmarshal(str, &a)

	fmt.Println(a)
}

func main() {
	f1()
}
