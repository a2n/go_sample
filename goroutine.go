package main

import (
    "fmt"
    "time"
    "math/rand"
    "runtime"
)

func f11() {
	ch := make(chan bool)
	go func(ch chan bool) {
		for i := 0; i < 5; i++ {
			fmt.Println(i)
			ch<-true
		}
		ch<-false
	}(ch)

	for {
		select {
		case v := <-ch:
			if v {
				fmt.Println(v)
			} else {
				return
			}
		}
	}
}

func f10() {
    ch := make(chan bool)
    slice := make([]int, 0)
    tick := time.Tick(500 * time.Millisecond)
    rand.Seed(time.Now().Unix())
    go func() {
	for {
	    <-tick
	    slice = append(slice, rand.Intn(10))
	    ch <- true
	}
    }()

    for {
	<-ch
	sum := 0
	for _, v := range slice {
	    sum += v;
	    if (sum > 42) {
		fmt.Println("Hit")
		slice = append(slice[:0], slice[len(slice) - 1:]...)
	    }
	}
	fmt.Printf("%v\n", slice)
    }
}

func f9() {
    NCPU := 2^5
    runtime.GOMAXPROCS(NCPU)
    n := 10
    ch := make(chan int, n)

    go func() {
	for i := 0; i < n; i++ {
	    ch <- i
	}
	close(ch)
    }()

    for n := range ch {
	fmt.Printf("%d, ", n)
    }
    fmt.Println()
}

func f81(ch chan bool) {
    <-time.After(100 * time.Millisecond)
    ch <- true
}

func f8() {
    NCPU := 10000
    runtime.GOMAXPROCS(NCPU)
    ch := make(chan bool)
    for i := 0; i < NCPU; i++ {
	go f81(ch)
    }

    for i := 0; i < NCPU; i++ {
	<-ch
    }
}

func f71(s []int, ch chan bool) {
    sum := 0
    for _, v := range s {
	for i := 0; i < v; i++ {
	    sum = rand.Int()
	}
    }

    fmt.Printf("%d\n", sum)
    ch <- true
}

func f7() {
    MAX := 10000000
    rand.Seed(time.Now().UnixNano())
    slice := make([]int, 0)
    for i := 0; i < MAX; i++ {
	slice = append(slice, rand.Int() % 10)
    }

    //NCPU := runtime.NumCPU()
    NCPU := 64
    fmt.Println(NCPU)
    runtime.GOMAXPROCS(NCPU)
    ch := make(chan bool)
    for i := 0; i < NCPU; i++ {
	go f71(slice[i * len(slice) / NCPU: (i + 1) * len(slice) / NCPU], ch)
    }

    for i := 0; i < NCPU; i++ {
	<-ch
    }
}

func ping() {
    ticker := time.NewTicker(3 * time.Second)
    for {
	select {
	case <-ticker.C:
	    fmt.Printf("%s\n", time.Now().String())
	}
    }
}

func f6() {
    ch := make(chan int)

    go func() {
	for {
	    fmt.Printf("%d, ", <-ch)
	}
    }()

    slice := []int { 0, 1, 2, 3, 4 }
    for i := 0; i < len(slice); i += 2{
	ch<- slice[i]
	ch<- slice[i + 1]
	fmt.Println()

	if len(slice[i + 2:]) < 2 {
	    break
	}
    }
    fmt.Printf("last: %d\n", slice[len(slice) - 1])
}

func f5() {
    ch := make(chan int)
    sum := 0
    for i := 0; i < 500; i++ {
	go func() {
	    for {
		<-time.After(1 * time.Second)
		rand.Seed(time.Now().UnixNano())
		ch<-rand.Int() % 10
	    }
	}()
	fmt.Printf("%d goroutines.\n", runtime.NumGoroutine())
	sum += <-ch
    }
    fmt.Printf("sum: %d\n", sum)
}

func f4() {
    slice := make([]bool, 0)
    for i := 0; i < 100; i++ {
	rand.Seed(time.Now().UnixNano())
	slice = append(slice, rand.Int() % 2 == 0)
    }
}

func f3() {
    const MAX = 10
    odd := make(chan int)
    even := make(chan int)

    go func() {
	for {
	    fmt.Printf("%d is odd.\n", <-odd)
	}
    }()

    go func() {
	for {
	    fmt.Printf("%d is even.\n", <-even)
	}
    }()

    for i := 0; i < MAX; i++ {
	rand.Seed(time.Now().UnixNano())
	v := rand.Int() % 10
	if v % 2 == 0 {
	    even <- v
	} else {
	    odd <- v
	}
    }

}

func f2() {
    const MAX = 10
    ch := make(chan uint32)

    go func() {
	for i := 0; i < MAX; i++ {
	    rand.Seed(time.Now().UnixNano())
	    v := rand.Uint32() % 10
	    ch<-v
	}
    }()

    slice := make([]uint32, 0)
    for i := 0; i < MAX; i++ {
	v := <-ch
	slice = append(slice, v)
    }
    fmt.Println(slice)
}

func f1() {
    fmt.Println("hello")
    f("a2n")
    go f("a2n")
}

func a() {
    f("direct")
    go f("goroutine")
    go func(msg string) {
	fmt.Println(msg)
    }("going")

    var input string
    fmt.Scanln(&input)
    fmt.Println("done")
}

func f(from string) {
    for i := 0; i < 3; i++ {
		fmt.Println(from, ": ", i)
    }
}

func main() {
    f11()
}
