package main

import (
    "fmt"
    "strings"
)

func main() {
    foo()
}

func foo() {
    records := make(map[string][]string, 0)

    key := "text1"
    records[key] = append(records[key], "regid1")
    records[key] = append(records[key], "regid2")

    key = "text2"
    records[key] = append(records[key], "regid3")
    records[key] = append(records[key], "regid4")

    for k, v := range records {
	fmt.Printf("%s: %s\n", k, strings.Join(v, ", "))
    }
}
